var request		= require('request');
var sha1		= require('node-sha1');
var config		= require('./config.js');
var db			= require('./db.js');
var sign 		= require('./sign.js');


var wechat = module.exports = {
	routes: function(app) {
		app.post("/wechat/configShare",		this.configShare);
		app.get("/wechat/login",			this.login);
		app.get("/wechat/oauth",			this.oauth);
		app.get("/wechat/refreshTokens",	this.refreshTokens);
		app.get("/wechat/getToken",			this.getToken);
	},

	startTokens: function() {
		if (config.ENV == 'DEVELOP')
			return;
		
		var refresh = function() {
			console.log('refresh()');
			request(config.BASE_URL + "wechat/refreshTokens", function(error, response, body) {
				setTimeout(refresh, 7000 * 1000);
			});
		}

		refresh();
	},

	getToken: function(req, res) {
		res.send(this.access_token);
	},

	refreshTokens: function(req, res) {
		console.log('refreshTokens():');
		// refresh access token
		var url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + config.WECHAT_APPID + "&secret=" + config.WECHAT_SECRET;
		request(url, function(err, response, body) {
			var obj = JSON.parse(body);
			console.log(obj);

			this.access_token = obj.access_token;

			// refresh jstock
			url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=' + this.access_token + '&type=jsapi';
			request(url, function(err, response, body) {
				var obj = JSON.parse(body);
				console.log(obj);
				this.jsTicket = obj.ticket;
				res.send("ok");
			}.bind(this));

		}.bind(this));
	},

	configShare: function(req, res) {
		var url		= req.body.url;
		// var ticket 	= 'sM4AOVdWfPE4DxkXGEs8VHkd16k_w3mQR_kSeaqmVm-MtaZ7ntST5DC-_f04sZo8-ESe0fP2D6-0YmfnvbFNLg';

		var ret = sign(this.jsTicket, url);
		ret.appId = config.WECHAT_APPID;
		console.log("configShare(): ");
		console.log(ret);
		res.send(ret);
	},

	login: function(req, res) {
		var scope, state, url;

		scope 	= (req.query.scope == 'snsapi_userinfo') ? "snsapi_userinfo":"snsapi_base";
		state 	= 'ugc';

		url 	= 	'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' +  
					config.WECHAT_APPID + 
					'&redirect_uri=' + 
					config.WECHAT_OAUTH_CB + 
					'&response_type=code&scope=' + 
					scope
					+ '&state=' +
					state +
					'#wechat_redirect';

		res.redirect(url);
	},

	oauth: function(req, res) {
		var code 	= req.query.code;
		var state	= req.query.state;
        console.log("oauth(): code = " + code);
        console.log("oauth(): state = " + state);

		// get token
		var url = 	'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' + 
					config.WECHAT_APPID + 
					'&secret=' + 
					config.WECHAT_SECRET + 
					'&code=' + 
					code +
					'&grant_type=authorization_code';

		console.log('oauth(): getting user token...');
		request(url, function(error, response, body) {
			var obj = JSON.parse(body);

			console.log(obj);

			// console.log(obj.access_token);
			// console.log(obj.expires_in);
			// console.log(obj.refresh_token);
			// console.log(obj.openid);
			// console.log(obj.scope);

			// get user info if scope userinfo
			if (obj.scope == "snsapi_userinfo") {
				console.log('TODO: getting user info');
			}

			// campaign pages according to state
			var campaign;
			switch (state) {
				case 'game2015':
					campaign = "http://wechat.sixpad.cn/#oid=" + obj.openid;
					break;
			}

			res.redirect(campaign);
		});
	}
};
