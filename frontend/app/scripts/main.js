var gender = 'male';

function genderBtn(g,e){
	$(e).parent().find('div').removeClass('selected');
	$('#'+g).addClass('selected');
	gender=g;
	console.log(gender);
}

function progress(percent, $element) {
	var progressBarWidth = percent * $element.width() / 100;
	// $element.find('div').animate({ width: progressBarWidth }, 500).html(percent + '%&nbsp;');
	$element.find('div').animate({
		width: progressBarWidth
	});
}

function preload()
{
  $('#preloadImg').html('');

  // set image list
  // images = new Array();

	images = [
		'./images/index.jpg',
		'./images/female.jpg',
		'./images/male_a.png',
		'./images/male_b.png',
		'./images/male_c.png',
		'./images/female_a.png',
		'./images/female_b.png',
		'./images/female_c.png',
		'./images/mask.png',
		'./images/sixpad.png',
		'./images/sixpad_o2.png',
		'./images/sixpad_o3.png',
		'./images/sixpad_o4.png',
		'./images/sixpad_o5.png',
		'./images/sixpad_o6.png'
		];

  var totalImg = images.length;

  //multi prelaod
  for (var i = 0; i < images.length; i++) {
    var preloadImg = new Image();
    preloadImg.src=images[i];
    $(preloadImg).addClass('preloadImg');
    preloadImg.setAttribute('id', 'preloadImg'+i);
    $('#preloadImg').append(preloadImg);
  }

  var preloadImgNum = 0;

  var preloadCount = function() {

    $('.preloadImg').load(function()
    {
      var id = $(this).attr('id');
      preloadImgNum++;

      var percent = Math.round(preloadImgNum/totalImg*100);
      progress(percent, $('#progressBar'));

      // console.log(preloadImgNum);

      if (preloadImgNum===totalImg) {
				page.change('index');
				// page.index();
      }
    });
  };
  preloadCount();
}

$('#card').on('swipe',function(){
	var select;
	if (gender==='male')
	{
		gender='female';
		select = true;
	}
	else{
		gender='male';
		select = false;
	}
	cardFlip(select);
});

function cardFlip(select){
	if (select===true) {
		gender='female';
		$('#card').flip(true);
	}
	else{
		gender='male';
		$('#card').flip(false);
	}
	console.log(gender);
	tracker('gender','selected '+gender);
	$('#face').attr('src','images/face_'+gender+'.png');
	$('.resultBefore').attr('src','images/result_'+gender+'_before.png');
	$('.resultAfter').attr('src','images/result_'+gender+'_after.png');
}

function reUpload(){
  $('#editControl').hide();
  $('#uploadCon').show();
	$('#uploadHide').show();
  $('#reUpload').hide();
	$('#faceCan').hide();
	$('#editBtn').hide();
  $('#backToGender').show();
  $('#reUpload').removeClass('inlineBlock');
	$('#resizeIcon').hide();
}

function overlay(e){
	$('#'+e).show();
}

function saveForm(){
	var name = $('#name').val();
	var phone = $('#phone').val();
	var email = $('#email').val();

	if (name!==''&&phone!=='')
	{
		$.post( serverURL + 'api/luckyDraw',
		{
			'oid':openid,
			'name': name,
			'phone': phone,
			'email':email
		}, function(res){
				if (res.save==='ok') {
					page.change('thankyou');
				}
			});
	}
}

function toWebsite(){
	window.location='http://sixpad.cn/';
}

function getReady() {
    alert(openid);
    // console.log('start preloading');
    var imageNames = [
			'./images/bg.jpg',
			'./images/resultBg.jpg',
			'./images/addimg.png',
			'./images/cd.png',
			'./images/cdBall.png',
			'./images/congrads.png',
			'./images/face_male.png',
			'./images/face_female.png',
			'./images/female_a.png',
			'./images/female_b.png',
			'./images/female_c.png',
			'./images/gender_female.png',
			'./images/gender_male.png',
			'./images/howToShare.png',
			'./images/index_logos.png',
			'./images/line.png',
			'./images/luckyDraw.png',
			'./images/male_a.png',
			'./images/male_b.png',
			'./images/male_c.png',
			'./images/luckyDraw.png',
			'./images/luckyDraw.png',
			'./images/mask.png',
			'./images/progressBg.png',
			'./images/resizeIcon.png',
			'./images/result_female_after.png',
			'./images/result_female_before.png',
			'./images/result_male_after.png',
			'./images/result_male_before.png',
			'./images/rotateBtn.png',
			'./images/rotateIcon.png',
			'./images/sixpadLogo.png',
			'./images/thankyou.png',
			'./images/timerIcon.png',
			'./images/tutorSixpad.png',
			'./images/welcome.png',
			'./images/zoomInBtn.png',
			'./images/zoomOutBtn.png',
			'./images/sixpad.png',
			'./images/sixpad_o2.png',
			'./images/sixpad_o3.png',
			'./images/sixpad_o4.png',
			'./images/sixpad_o5.png',
			'./images/sixpad_o6.png'
    ];
    var imagesCount = imageNames.length;
    var resourceCount = imagesCount;
    var loadedResourceCount = 0;
    var images = [];
    for (var i = 0; i < imagesCount; i++) {
        images[i] = new Image();
        images[i].src = imageNames[i];
        images[i].onload = function () {
            var progress = Math.ceil(100 * (++loadedResourceCount / resourceCount));
            // console.log(progress);
            $('#progress').text(progress + '%');
            drawCircle('#cd4a00', progress / 100);
            if (loadedResourceCount >= resourceCount) {
								page.change('index');
                // console.log('end');
            }
        };
    }
}

var el = document.getElementById('loading-canvas');
var canvas = document.createElement('canvas');
var ctx = canvas.getContext('2d');
canvas.width = canvas.height = 200;
el.appendChild(canvas);
ctx.translate(200 / 2, 200 / 2);
ctx.rotate((-1 / 2 + 0 / 180) * Math.PI);
var drawCircle = function (color, percent) {
    percent = Math.min(Math.max(0, percent || 1), 1);
    ctx.beginPath();
    ctx.arc(0, 0, (200 - 5) / 2, 0, Math.PI * 2 * percent, false);
    ctx.strokeStyle = color;
    ctx.opacity = 0.1;
    ctx.lineCap = 'round';
    ctx.lineWidth = 5;
    ctx.stroke();
};

function tracker(p,e)
{
	console.log("tracker= page:" + p + " / button: " + e);
	_hmt.push(['_trackEvent', 'button', p , e]);
}

function getRanking()
{
	$.get( serverURL + 'api/ranking', function(result)
	{
		var mintime;
		var mean;
		$('.minTime').html(result.minTime);
		$('.meanTime').html(result.mean);
	});
}
