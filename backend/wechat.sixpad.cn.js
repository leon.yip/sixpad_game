var express		= require('express');
var config 		= require('./config.js');
var wechat		= require('./wechat.js');
var api			= require('./api.js');
var db			= require('./db.js');
var app			= express();

var initApp = function() {
	app.configure(function() {
		app.use(express.json({limit:'10mb'})); 
		app.use(express.urlencoded({limit:'10mb'}));
		app.use(function(req, res, next) {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			next();
		});

		app.use(function(req, res, next) {
			console.log('%s %s %s %s', req.ip, req.method, req.url, req.path);
			next();
		});

		wechat.routes(app);
		api.routes(app);
		app.use('/',					express.static(__dirname + "/public/"));
	});

	app.listen(config.PORT);
	console.log('listening on ' + config.PORT + "...");

	// wechat tockets
	wechat.startTokens();	
}

db.connect(initApp);
