var config		= require('./config.js');
var dan			= require('./dan.js');
var db			= require('./db.js');
var ObjectID = require('mongodb').ObjectID;

var api = module.exports = {
	routes: function(app) {
		app.get("/api/test", 			this.test);
		app.post("/api/saveProfile",	this.saveProfile);
		app.post("/api/getProfile",		this.getProfile);
		app.post("/api/luckyDraw",		this.luckyDraw);
		app.get("/api/ranking",		this.ranking);
	},

	test: function(req, res) {
		res.send('api test');
	},

	saveProfile: function(req, res) {
		var gender	= req.body.gender,
			time	= parseFloat(req.body.time),
			imageData	= req.body.imageData;

		// get user id somehow
		var obj = {
			added		: new Date(),
			userImage	: imageData?1:0,
			gender	: gender,
			time	: time
			};

		db.mongo
			.collection("profile")
			.insert(obj, function(err) {
				var fn = config.IMAGE_PATH + obj._id + ".png";

				if (imageData) {
					dan.writeFile(imageData, fn, function() {
						res.send({	designId: obj._id});
					});
					return;
				}
				res.send({	designId: obj._id});
			});
	},

	getProfile: function(req, res) {
		var designId = req.body.designId;

		db.mongo
			.collection('profile')
			.find({_id:new ObjectID(designId)})
			.toArray(function(err, items) {

				for (var i=0; i<items.length; i++) {
					var item = items[i];
					var obj = {
						designId 	: item._id,
						gender 	: item.gender,
						time 	: item.time
					};
					res.send(obj);
				}
			});
	},

	ranking: function(req, res){
		db.mongo
			.collection('profile')
			.aggregate(
			 {
					$group:
					{
						"_id": null,
						"meanTime": { $avg: "$time" },
						"min":{$min:"$time"}
					}
				},
	   function(err, items){
			 for (var i=0; i<items.length; i++)
			 {
					var item = items[i];
					var obj = {
						minTime	: item.min,
						mean : item.meanTime.toFixed(2)
					};
					res.send(obj);
				}
			});
	},

	luckyDraw: function(req, res) {
		var name			= req.body.name,
			phone			= req.body.phone,
			email			= req.body.email;

		// get user id somehow
		var obj = {
			added		: new Date(),
			name:	name,
			phone:	phone,
			email:	email
			};

		db.mongo
			.collection("luckyDraw")
			.insert(obj, function(err) {
				res.send({	save: 'ok'});
			});
	},
};
