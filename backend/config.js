var SETTINGS = {
	// develop
	DEVELOP: {
		PORT 			: 1500,
		BASE_URL	 	: "http://localhost:1500/",
		DB_URI			: "mongodb://localhost:27017/sixpad",
		IMAGE_PATH	    : "public/profileImg/",
		WECHAT_APPID	: "wxc9d6687423232381",
		WECHAT_SECRET 	: "1c12937e39935713009bd7bb091cbe48"
	},

	// staging
	STAGING: {
		PORT 			: 1501,
		BASE_URL	 	: "http://sp.lee-create.com/",
		DB_URI			: "mongodb://localhost:27017/sixpad-staging",
		WECHAT_APPID	: "wxc9d6687423232381",
		WECHAT_SECRET 	: "1c12937e39935713009bd7bb091cbe48"
	},

	// production
	PRODUCTION: {
		PORT 			: 1502,
		BASE_URL	 	: "http://sp.lee-create.com/",
		DB_URI			: "mongodb://localhost:27017/sixpad",
		IMAGE_PATH	    : "public/profileImg/",
		WECHAT_APPID	: "wx22599f2724dc2734",
		WECHAT_SECRET 	: "9d96297abf7285e66fb0e654a2930ba0"
	}
}

var config = module.exports = {
	init: function() {
		var v = process.env.ENV;

		if (v != "STAGING" && v != "PRODUCTION") {
			v = "DEVELOP";
			console.log("For staging and production: \n");
			console.log("   ENV=STAGING node server.js");
			console.log("   ENV=STAGING forever start server.js\n");
		}

		this.ENV = v;

		for (k in SETTINGS[v]) {
			this[k] = SETTINGS[v][k];
		}
	}
};

config.init();
