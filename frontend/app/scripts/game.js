var game = {
	init:function(){
    page.change('game');
		game.isover=false;
		game.muscleStage = [10,20,30];
		game.finishScore = 30;
    game.scoreNumber = 0;
		game.gameTimer = 0;
		game.curTarget = 0;
		game.count = 0;
		game.comboArray = [1,2];
		game.combo = 1;
		var timer;
		$('#curScore').html(game.finishScore);
		$('#upperCd').height(windowHeight/2);
		$('#lowerCd').height(windowHeight/1.66);

		game.gameStage = new Kinetic.Stage({
			container: 'gameCon',
			x:width/2,
			y:width/2,
			width: width,
			height: windowHeight-$('#scoreBar').height(),
			offset:{
				x:width/2,
				y:width/2
			}
		});

		game.headLayer = new Kinetic.Layer();
		game.bodyLayerA = new Kinetic.Layer();
		game.bodyLayerB = new Kinetic.Layer();
		game.bodyLayerC = new Kinetic.Layer();
		game.sixpadLayer = new Kinetic.Layer();
		game.sixpadLayer01 = new Kinetic.Layer();
		game.sixpadLayer02 = new Kinetic.Layer();
		game.sixpadLayer03 = new Kinetic.Layer();
		game.sixpadLayer04 = new Kinetic.Layer();
		game.sixpadLayer05 = new Kinetic.Layer();
		game.sixpadLayer06 = new Kinetic.Layer();
		game.profileImg = new Image();
		game.bodyImgA = new Image();
		game.bodyImgB = new Image();
		game.bodyImgC = new Image();
		game.sixpadImg = new Image();
		game.sixpadImg01 = new Image();
		game.sixpadImg02 = new Image();
		game.sixpadImg03 = new Image();
		game.sixpadImg04 = new Image();
		game.sixpadImg05 = new Image();
		game.sixpadImg06 = new Image();

		game.profileImg.src = profileData;
		game.bodyImgA.src = './images/' + gender + '_a.png';
		game.bodyImgB.src = './images/' + gender + '_b.png';
		game.bodyImgC.src = './images/' + gender + '_c.png';
		game.sixpadImg.src = './images/sixpad.png';
		game.sixpadImg01.src = './images/sixpad_o1.png';
		game.sixpadImg02.src = './images/sixpad_o2.png';
		game.sixpadImg03.src = './images/sixpad_o3.png';
		game.sixpadImg04.src = './images/sixpad_o4.png';
		game.sixpadImg05.src = './images/sixpad_o5.png';
		game.sixpadImg06.src = './images/sixpad_o6.png';

		// game.bodyImgA.onload = function() {
		var head = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)-(width*110/400/2),
			y:width*65/400,
			image: game.profileImg,
			width: width*110/400,
			height: width*110/400
		});
		var bodyA = new Kinetic.Image({
			x: 0,
			y: 30,
			image: game.bodyImgA,
			width: width,
			height: width,
		  align: 'center'
		});
		var bodyB = new Kinetic.Image({
			x: 0,
			y: 30,
			image: game.bodyImgB,
			width: width,
			height: width,
		  align: 'center'
		});
		var bodyC = new Kinetic.Image({
			x: 0,
			y: 30,
			image: game.bodyImgC,
			width: width,
			height: width,
		  align: 'center'
		});
		var sixpad = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)-(width*95/400/2),
			y: width*205/400,
			image: game.sixpadImg,
			width: width*100/400,
			height: width*100/400,
		  align: 'center'
		});

		var sixpad01 = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)-(width*67/400/2),
			y: width*214/400,
			image: game.sixpadImg01,
			width: width*30/400,
			height: width*30/400
		});
		sixpad01.on('touchstart', function() {
			if (game.count<0) {
				game.count=0;
			}
			game.count++;
			game.score();
			console.log('count ' + game.count);
		});
		sixpad01.on('touchend', function() {
			game.count--;
			console.log('count ' + game.count);
		});

		var sixpad02 = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)+(width*17/400/2),
			y: width*214.5/400,
			image: game.sixpadImg02,
			width: width*30/400,
			height: width*30/400
		});
		sixpad02.on('touchstart', function() {
						if (game.count<0) {
							game.count=0;
						}
			game.count++;
			console.log('count ' + game.count);
			game.score();
		});
		sixpad02.on('touchend', function() {
			game.count--;
			console.log('count ' + game.count);
		});

		var sixpad03 = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)-(width*75/400/2),
			y: width*241.5/400,
			image: game.sixpadImg03,
			width: width*27/400,
			height: width*27/400
		});
		sixpad03.on('touchstart', function() {
						if (game.count<0) {
							game.count=0;
						}
			game.count++;
			console.log('count ' + game.count);
			game.score();
		});
		sixpad03.on('touchend', function() {
			game.count--;
		});

		var sixpad04 = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)+(width*30/400/2),
			y: width*240.1/400,
			image: game.sixpadImg04,
			width: width*27/400,
			height: width*27/400
		});
		sixpad04.on('touchstart', function() {
						if (game.count<0) {
							game.count=0;
						}
			game.count++;
			console.log('count ' + game.count);
			game.score();
		});
		sixpad04.on('touchend', function() {
			game.count--;
			console.log('count ' + game.count);
		});

		var sixpad05 = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)-(width*64/400/2),
			y: width*265/400,
			image: game.sixpadImg05,
			width: width*28/400,
			height: width*28/400
		});
		sixpad05.on('touchstart', function() {
						if (game.count<0) {
							game.count=0;
						}
			game.count++;
			console.log('count ' + game.count);
			game.score();
		});
		sixpad05.on('touchend', function() {
			game.count--;
			console.log('count ' + game.count);
		});

		var sixpad06 = new Kinetic.Image({
			x: (game.gameStage.getWidth()/2)+(width*19/400/2),
			y: width*265/400,
			image: game.sixpadImg06,
			width: width*28/400,
			height: width*28/400
		});
		sixpad06.on('touchstart', function() {
						if (game.count<0) {
							game.count=0;
						}
			game.count++;
			console.log('count ' + game.count);
			game.score();
		});
		sixpad06.on('touchend', function() {
			game.count--;
			console.log('count ' + game.count);
		});

		// add the shape to the layer
		game.headLayer.add(head);
		game.bodyLayerA.add(bodyA);
		game.bodyLayerB.add(bodyB);
		game.bodyLayerC.add(bodyC);
		game.sixpadLayer.add(sixpad);
		game.sixpadLayer01.add(sixpad01);
		game.sixpadLayer02.add(sixpad02);
		game.sixpadLayer03.add(sixpad03);
		game.sixpadLayer04.add(sixpad04);
		game.sixpadLayer05.add(sixpad05);
		game.sixpadLayer06.add(sixpad06);
		// add the layer to the stage
		game.gameStage.add(game.bodyLayerA);
		game.gameStage.add(game.bodyLayerB);
		game.gameStage.add(game.bodyLayerC);
		game.gameStage.add(game.headLayer);
		game.gameStage.add(game.sixpadLayer);
		game.gameStage.add(game.sixpadLayer01);
		game.gameStage.add(game.sixpadLayer02);
		game.gameStage.add(game.sixpadLayer03);
		game.gameStage.add(game.sixpadLayer04);
		game.gameStage.add(game.sixpadLayer05);
		game.gameStage.add(game.sixpadLayer06);

		game.headLayer.hide();
		game.gameStage.hide();

		$('#upperCd').animate({'top':-windowHeight},6000,function() {
    // Animation complete.
	  });
		$('#lowerCd').animate({'bottom':-windowHeight},6000,function() {
    // Animation complete.
	  });

    this.countDown = function ()
		{
			var time = 3;
      game.counter = setInterval(function () {
				time--;
				if (time===0) {
					$('#ballText').addClass('start');
					$('#ballText').html('Start');
				}
				else if (time<0) {
					clearInterval(game.counter);
					game.start();
				}
				else
				{
					$('#ballText').html(time);
	        console.log('count down time: ' + time);
				}
      }, 1000);
	  };
		game.countDown(3);
	},
	start:function(){
		$('#cdBallCon').fadeOut();
		$('#cdCon').hide();
		game.pickTarget();
		game.gameStage.setScale({x:3,y:3});
		game.bodyLayerB.hide();
		game.bodyLayerC.hide();
		game.gameStage.show();
		game.gameStage.draw();
		game.startTimer();
		// game.zoom();
	},
	startTimer:function(){
    this.timer = function (time)
		{
			game.sec = 0;
			game.hundredth = 0;
			game.tenth = 0;

      game.gameTimeout = setInterval(function () {
				game.tenth++;

				if (game.tenth===10)
				{
					game.tenth=0;
					game.hundredth++;
				}

				if (game.hundredth===10) {
					game.hundredth=0;
					game.sec++;
				}

				game.gameTimer = game.sec + '.' + game.hundredth + game.tenth;

				$('#timeUse').html(game.gameTimer);
      }, 10);
	  };
		game.timer(0);
	},
	pickTarget:function(){
		game.padArray=[1,2,3,4,5,6];

		if (game.count > game.combo || game.count < 0) {
			game.combo = 0;
		}

		game.combo = game.comboArray[Math.floor(Math.random()*game.comboArray.length)];
		console.log('game Combe = ' + game.combo);

		if (game.combo==2) {

			var target1 = game.padArray.splice(Math.floor(Math.random()*game.padArray.length), 1);

			var target2 = game.padArray[Math.floor(Math.random()*game.padArray.length)];

			console.log('target1 '+ target1 + ' target2 '+ target2);

			game.sixpadLayer01.hide();
			game.sixpadLayer02.hide();
			game.sixpadLayer03.hide();
			game.sixpadLayer04.hide();
			game.sixpadLayer05.hide();
			game.sixpadLayer06.hide();

			game['sixpadLayer0'+target1].show();
			game['sixpadLayer0'+target2].show();
		}
		else{
			var target = game.padArray[Math.floor(Math.random()*game.padArray.length)];

			if (target === game.curTarget) {
				game.pickTarget();
			}
			else{
				game.curTarget = target;

				game.sixpadLayer01.hide();
				game.sixpadLayer02.hide();
				game.sixpadLayer03.hide();
				game.sixpadLayer04.hide();
				game.sixpadLayer05.hide();
				game.sixpadLayer06.hide();
				game['sixpadLayer0'+target].show();
			}
		}
		// game.count = 0;
	},
	score:function(){
		if (game.isover===false) {
			if (game.count == game.combo) {
				if (game.count < 0 || game.count > 0) {
					game.count=0;
				}
				game.pickTarget();
				game.scoreNumber++;
				var curScore = game.finishScore - game.scoreNumber;
				$('#curScore').html(curScore);

				var percent = Math.round(game.scoreNumber/game.finishScore*100);
				var progressBarWidth = percent * $('#targetBar').width() / 100;

				$('#targetBar').find('div').width(progressBarWidth);

				$('#score span').html(game.scoreNumber);
				if (game.scoreNumber===game.muscleStage[0])
				{
					console.log('B');
					game.bodyLayerA.hide();
					game.bodyLayerB.show();
				}
				else if (game.scoreNumber===game.muscleStage[1]) {
					console.log('C');
					game.bodyLayerB.hide();
					game.bodyLayerC.show();
				}
				else if (game.scoreNumber===game.muscleStage[2]) {
					game.over();
				}
				game.gameStage.draw();
			}
		}
	},
	zoom:function(){
		var zoomAnimation = new Kinetic.Animation(function (frame)
		{
			var period = 4000;
			var amplitude = 150;
			var scale = (frame.time * 2 * Math.PI / period);
			if (scale>=2) {
				zoomAnimation.stop();
				game.startTimer();
				return;
			}
			game.gameStage.setScale({x:scale+1,y:scale+1});
			game.gameStage.draw();
		}, layer);
		zoomAnimation.start();
	},
	over:function(){
		game.isover=true;
		// var zoomOutAnimation = new Kinetic.Animation(function (frame)
		// {
		// 	var period = 4000;
		// 	var amplitude = 150;
		// 	var scale = (frame.time * 2 * Math.PI / period);
		// 	if (scale>=2) {
		// 		zoomOutAnimation.stop();
		// 	}
		// 	game.gameStage.setScale({x:2-scale+1,y:2-scale+1});
		// 	game.sixpadLayer01.show();
		// 	game.sixpadLayer02.show();
		// 	game.sixpadLayer03.show();
		// 	game.sixpadLayer04.show();
		// 	game.sixpadLayer05.show();
		// 	game.sixpadLayer06.show();
		// 	game.gameStage.draw();
		// }, layer);
		// zoomOutAnimation.start();
		// clearTimeout(game.gameTimeout);

		clearInterval(game.gameTimeout);
		console.log('Done');
		console.log('Total Time: '+ game.gameTimer);
		$('.resultTime').html(game.gameTimer);
    saveProfile(profileData,game.gameTimer);
		page.change('result');
	},
};

function saveProfile(profileData,time){
	console.log(game.gameTimer);

	$.post( serverURL + 'api/saveProfile',
	{
    'gender': gender,
		'imageData': profileData,
		'time': game.gameTimer
	}, function(res){
		  designId = res.designId;

			getRanking();

			wx.onMenuShareAppMessage({
			    title: '这是我的Sixpad', // 分享标题
			    desc: '快来看看我的Sixpad', // 分享描述
			    link: wxData.link + '#designid=' + designId, // 分享链接
			    imgUrl: wxData.imgUrl, // 分享图标
			    type: '', // 分享类型,music、video或link，不填默认为link
			    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
			    success: function () {},
			    cancel: function () {}
			});

			wx.onMenuShareTimeline({
			    title: '大家一起体验吧！', // 分享标题
			    link: wxData.link + '#designid=' + designId, // 分享链接
			    imgUrl: wxData.imgUrl, // 分享图标
			    success: function () {},
			    cancel: function () {}
			});

    });
}
