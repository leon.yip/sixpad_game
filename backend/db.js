var mongo		= require('mongodb');
var ObjectID	= require('mongodb').ObjectID;
var config		= require('./config.js');

var db = module.exports = {
	// db
	connect: function(cb) {
		console.log('db.connect(): connecting to mongo...');

		mongo.Db.connect(config.DB_URI, function(err, db) {
			if (err) throw err;

			this.mongo = db;
			console.log('db.connectDB(): mongo connected!');

			cb();
		}.bind(this));
	},

	getObjectID: function(objectId) {
		return new ObjectID(objectId);
	}
};
