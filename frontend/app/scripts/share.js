var wxData =
{
  imgUrl	: 'http://sp.lee-create.com/images/sixpad.png',
  link		: 'http://sp.lee-create.com/',
  desc		: '邀请大家一起体验吧！',
  title		: '体验Sixpad！'
};

var initShare = function() {
	var setupWx = function(data) {
		wx.config({
		  debug: false,
		  appId: data.appId,
		  timestamp: data.timestamp,
		  nonceStr: data.nonceStr,
		  signature: data.signature,
		  jsApiList: ['onMenuShareAppMessage','onMenuShareTimeline']
		});

		wx.error(function(res){
			alert(JSON.stringify(res));
		});

		wx.ready(function () {
			wx.onMenuShareAppMessage({
			    title: wxData.title, // 分享标题
			    desc: wxData.desc, // 分享描述
			    link: wxData.link, // 分享链接
			    imgUrl: wxData.imgUrl, // 分享图标
			    type: '', // 分享类型,music、video或link，不填默认为link
			    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
			    success: function () {},
			    cancel: function () {}
			});

			wx.onMenuShareTimeline({
			    title: wxData.desc, // 分享标题
			    link: wxData.link, // 分享链接
			    imgUrl: wxData.imgUrl, // 分享图标
			    success: function () {},
			    cancel: function () {}
			});
		});
	};

	// get wechat share config
	var url = encodeURI(location.href.split('#')[0]);
		$.post('http://wechat.sixpad.cn/wechat/configShare',
			{url:url},
			function(data) {
				setupWx(data);
			});
};
