var openid;
var hash;
var param;
var designid;

//get date and time
var date = new Date();
var days = 365;
date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

var page = {
    index: function () {
        window.location.hash = '#index';
        $('.page').hide();
        $('#index').show();
    },
    change: function (page) {
        window.location.hash = '#' + page;
        $('.overlay').hide();
        $('.page').hide();
        $('#' + page).show();

        if (page === 'gender') {
            var ch = $('.front').find('img').height();
            // $('#card').css({'margin-top':-ch});
        }
    },
    show: function (page) {
        $('.overlay').hide();
        $('#' + page).show();
    },
    hide: function (page) {
        $('#' + page).hide();
    }
};

function getHashAndOid() {
    hash = window.location.hash.substring(1);
    if (hash.indexOf('designid') > -1) {
        designId = hash.replace('designid=', '');
        $('#designId').html(designId);
        page.change('share');
        getDesignById();
        getRanking();
    } else if (hash.indexOf('oid') > -1) {
        openid = hash.replace('oid=', '');
        document.cookie = 'openid=' + openid + '; expires=' + date;
        page.change('index');
    } else {
         checkCookie();
        drawCircle('#4f1c00', 100 / 100);
        getReady();
    }

    initShare();
}

function getDesignById() {
    $.post(serverURL + 'api/getProfile', {
        'designId': designId
    }, function (res) {
        var gender = res.gender;
        $('.resultBefore').attr('src', 'images/result_' + gender + '_before.png');
        $('.resultAfter').attr('src', 'images/result_' + gender + '_after.png');
        $('.resultTime').html(res.time);
        $('.profileImg').attr('src', serverURL + 'profileImg/' + designId + '.png');
    });
}

$(window).on('hashchange', function () {
    hash = window.location.hash.substring(1);
    if (hash === '') {
        page.index();
    } else {
        page.change(hash);
    }
});

function checkCookie() {
    openid = getCookie('openid');
    hash = window.location.hash;

    if (!openid) {
        //go to auth
        console.log('go to auth');
        window.location = wechatLogin;
    } else if (openid) {
        // param = getParam('');
        // preload();
        drawCircle('#4f1c00', 100 / 100);
        getReady();
        // initShare();
    }
}

function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == '') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return '';
}

function getParam(name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var regexS = '[\\?&]' + name + '=([^&#]*)';
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results === null)
        return;
    else
        return results[1];
}